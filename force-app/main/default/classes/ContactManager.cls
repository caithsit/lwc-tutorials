public with sharing class ContactManager 
{
    @AuraEnabled(cacheable=true)
    public static list<Contact> getContacts(Integer numberOfRecords)
    {
        return [SELECT Id, LastName, Phone FROM Contact LIMIT :numberOfRecords];    
    }
}
