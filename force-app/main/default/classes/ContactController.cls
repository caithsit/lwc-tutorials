public with sharing class ContactController 
{
    public ContactController() 
    {

    }

    @AuraEnabled
    public static List<Contact> findAll()
    {
        return [SELECT Id, name, phone 
                FROM Contact LIMIT 50];
    }

    @AuraEnabled
    public static List<Contact> findByName(String searchKey)
    {
        System.debug('Inside');
        String name = '%' + searchKey + '%';

        return [SELECT Id, name, phone 
                FROM Contact 
                WHERE name LIKE: name LIMIT 50];
    }

    @AuraEnabled
    public static Contact findById(String contactId)
    {
        Contact cont = [SELECT Id, name, phone, mobilephone, Account.Name  
                        FROM  Contact 
                        WHERE Id =: contactId ];
        System.debug('findById method ' + cont);
        return cont;
    }
}
