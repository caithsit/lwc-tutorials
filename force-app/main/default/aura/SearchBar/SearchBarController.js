({
    searchKeyChange : function(component, event, helper) 
    {
        var myEvent=$A.get("e.c:SearchKeyChangeEvent");
        myEvent.setParams({"searchKey":event.target.value});
        myEvent.fire();
        console.log('Event Fired ' + event.target.value);
    }
})
