({
    locationChangeFunction : function(component, event, helper) 
    {  
        var token=event.getParam("token");
        if (token) 
        {   
            if (token.indexOf('contact/')===0) // is it in the URL?
            {
                var contactId = token.substr(token.indexOf('/')+1);
                var action=component.get("c.findById");
    
                action.setParams({"contactId":contactId});
            }
    
            action.setCallback(this, function(a){
                console.log('Callback: ' + a.getReturnValue());
                component.set("v.contact", a.getReturnValue());
            });
    
            $A.enqueueAction(action);
        }
        
    }
})
