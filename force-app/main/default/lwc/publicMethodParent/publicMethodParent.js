import { LightningElement, track } from 'lwc';

export default class PublicMethodParent extends LightningElement 
{
    @track value;

    checkboxSelectHandler()
    {
        const childComponent = this.template.querySelector('c-public-method-child'); 
                            //query Selector uses CSS selectors and returns first element that matches the selector
        const returnedMessage = childComponent.selectCheckbox(this.value);
        console.log('Message received ' + returnedMessage);
    }

    onInputChangeHandler(event)
    {
        this.value = event.target.value;
    }

    handleChangeEvent(event)
    {
        this.template.querySelector('c-public-method-child').changeMessage(event.target.value);
    }
}