import { LightningElement, track } from 'lwc';

export default class SimpleInterestCalculator extends LightningElement 
{
    @track currentOutput = 'test';
    principal;
    rateOfInterest;
    noOfYears;

    principalChangeHandler(event)
    {
        this.principal = parseInt(event.target.value);
    }

    timeChangeHandler(event)
    {
        this.noOfYears = parseInt(event.target.value);
    }

    rateChangeHandler(event)
    {
        this.rateOfInterest = parseInt(event.target.value);

    }

    calcSIHandler()
    {
        calculation;
        this.calculation = (this.principal * this.rateOfInterest * this.noOfYears) / 100;
        this.currentOutput = 'The Simple Interest is : ' + this.calculation;
    }
        
}