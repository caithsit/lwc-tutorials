import { LightningElement, track} from 'lwc';

export default class EmployeeDetailsParent extends LightningElement 
{
    @track value;


    empDetails=[
        { empName:'Steve', empAddress:'NC'},
        { empName:'Smith', empAddress:'TX'},
        { empName:'Raymond', empAddress:'TN'}
    ];
/*
    checkboxSelectHandler()
    {
        // querySelector() uses CSS selectors and returns the first element within the 
        // document that matches the specified selector
        const childComponent = this.template.querySelector('c-employee-detail');
        const returnMessage = childComponent.selectCheckbox(this.value);
        console.log(returnMessage);
    }

    onValueChangeHandler()
    {
        this.value = event.target.value;
    }*/
}