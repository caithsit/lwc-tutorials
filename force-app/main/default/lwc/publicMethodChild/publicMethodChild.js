import { LightningElement, track, api } from 'lwc';

export default class PublicMethodChild extends LightningElement 
{
    @track value = ['red']; // the value will be priva reactive property (will change and using track)

    options = // this is private non reactive property because we don't expect it to change
    [
            { label: 'Red Marker', value: 'red' },
            { label: 'Blue Marker', value: 'blue'},
            { label: 'Green Marker', value: 'green'},
            { label: 'Black Marker', value: 'black'}
    ];
    

    @api
    selectCheckbox(checkboxValue)
    {
        const selectedCheckbox = this.options.find( checkbox => { // checbox is the name of the parameter
            return  checkboxValue === checkbox.value;// This will return a value only if the checkboxValue is equal to a value of this checkbox
        })

        if (selectedCheckbox) 
        {
            this.value = selectedCheckbox.value; 
            return "Successfully checked";
        } else {
            return "No checkbox found";
        }
    }

    @track message = 'Test';

    @api
    changeMessage(strStrig)
    {
        this.message = strStrig.toUpperCase();
    }
}