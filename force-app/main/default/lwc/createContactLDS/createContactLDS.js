import { LightningElement,track,wire,api } from 'lwc';
import { createRecord,getRecord} from 'lightning/uiRecordApi';

const fieldArray=['Contact.FirstName','Contact.LastName','Contact.Phone','Contact.Email'];

export default class CreateContactLDS extends LightningElement 
{
    @track contactName; // reactive properties
    @track contactFirstName;
    @track contactPhone;
    @track contactEmail;
    @track recordId;
    @track createdRecordId;
    @api accountRecordId = '0013h000005qVfvAAE'; 
    @api contactRecordId = '0033h000005H8SiAAK'; //Remember @api are public and reactive by 

    @wire(getRecord,{recordId:'$recordId',fields:fieldArray}) contactRecord;


    contactFirstNameChangeHandler(event)
    {
        this.contactFirstName = event.target.value;
    }
    
    contactNameChangeHandler(event)
    {
        this.contactName = event.target.value;
    }
    
    contactEmailChangeHandler(event)
    {
        this.contactEmail = event.target.value;
    }

    contactPhoneChangeHandler(event)
    {
        this.contactPhone = event.target.value;
    }


    createContactDataService()
    {
        const fields = {    'FirstName':this.contactFirstName, 
                            'LastName':this.contactName,
                            'Phone':this.contactPhone, 
                            'Email':this.contactEmail};

        const recordInput={apiName:'Contact',fields};

        createRecord(recordInput).then(response=>{
            console.log('Contact has been created successfully', response.id); 
            this.recordId=response.id;
        }).catch(error=>{
            console.log('Error in creating contact: ', error.body.message);
        });

    }


    createContactBaseComponents(event) 
    {
        // we are getting the ID from the Contact that was just created, because this.createdRecordID is
        // reactive then the component refreshes automatically.
        this.createdRecordId = event.detail.id; 
    }
    
    
    get retContactFirstName()
    {
        if (this.contactRecord.data) {
            return this.contactRecord.data.fields.FirstName.value;
        }
    }
    
    get retContactName()
    {
        if (this.contactRecord.data)
        {
            return this.contactRecord.data.fields.LastName.value;
        } 
        return undefined;
    }

    get retContactPhone()
    {
        if (this.contactRecord.data)
        {
            return this.contactRecord.data.fields.Phone.value;
        }
        return undefined;
    }

    get retContactEmail()
    {
        if (this.contactRecord.data)
        {
            return this.contactRecord.data.fields.Email.value;
        }
    }
}