
import { LightningElement } from 'lwc';

export default class ChildComponent2 extends LightningElement {
    handleChange(event)
    {
        const name=event.target.value;
        const selectEvent=new CustomEvent('mycustomevent',{detail:name,bubbles:true});
        this.dispatchEvent(selectEvent);
    }
}
/*import { LightningElement } from 'lwc';

export default class ChildComponentCustomEvents extends LightningElement 
{
    handleChange(event)
    {
        const name = event.target.value;
        console.log('Test ' + name);
        const selectEvent = new CustomEvent('mycustomevent',{detail:name});//Creates event
        console.log('SO FAR ' + selectEvent);
        this.dispatchEvent(selectEvent); // dispatches event
        console.log('So good');

    }

}*/