//Child component JS file

import { LightningElement, track, api} from 'lwc'; //

export default class EmployeeDetail extends LightningElement 
{
    @api empDetail = {empName: 'John',empAddress:'TX'}  // public reactive property so it requires the @api declarator
    @api show=false; // This is a public and reactive property in a child component. It defaults to FALSE and can only be updated from the parent.
    
   /* @track value = ['red'];

    options = 
    [
        { label:"Red Marker", value:'red'},
        { label:"Green Marker", value:'gree'},
        { label:"Blue Marker", value:'blue'}
    ];
           

    @api 
    selectCheckbox(checkboxValue) // @api defines a public method in the component
    {
        // The fins method returns the value of the first element in the array that satisfies
        // the provided testing function
        const selectedCheckbox = this.options.find( checkbox => { 
            return checkboxValue === checkbox.value;
        })

        if(selectedCheckbox) // if selected checkbox found
        {
            this.value = selectedCheckbox.value;
            return "Successfully checked";
        } else {
            return "No checkbox found";
        }
    }*/
}