import { LightningElement,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigationServiceLWCDemo extends NavigationMixin(LightningElement) 
{
    //@api recordId; we should use this line if we put this component in a RecordPage but because I am lazy and want to see everything in one
    // place then I am choosing to hardcode this account lol 
    @api recordId = '0013h000005qVfvAAE';

    navigateToNewRecordPage()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__recordPage',
            attributes:{
                "recordId":this.recordId,
                "objectApiName":"Account",
                "actionName":"new"
            }
        });
    }
    
    navigateToEditRecordPage()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__recordPage',
            attributes: {
                "recordId":this.recordId,
                "objectApiName":"Account",
                "actionName":"edit"
            }
        });
    }

    navigateToViewRecordPage()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__recordPage',
            attributes: {
                "recordId":this.recordId,
                "objectApiName":"Account",
                "actionName":"view"
            }
        });
    }

    navigateAccRecentView() 
    {
        this[NavigationMixin.Navigate]({
            type:'standard__objectPage',
            attributes: {
                "objectApiName":"Account",
                "actionName":"list"
            },
            "state":{
                "filterName":"Recent"
            }
        });
    }

    navigateRelatedListView()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__recordRelationshipPage',
            attributes: {
                "recordId":this.recordId,
                "objectApiName":"Account",
                "relationshipApiName":"Contacts",
                "actionName":"view"
            }
        });
    }

    navigateAccObject()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__objectPage',
            attributes: {
                "objectApiName":"Account",
                "actionName":"home"
            }
        });
    }

    navigateConObject()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__objectPage',
            attributes: {
                "objectApiName":"Contact",
                "actionName":"home"
            }
        });
    }

    navigateToWebObject()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__webPage',
            attributes: {
                "url":"https://www.facebook.com/"
            }
        });
    }

    navigationToHomePage()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__namedPage',
            attributes: {
                "pageName":"home"
            }
        });
    }

    navidationToChatterHome()
    {
        this[NavigationMixin.Navigate]({
            type:'standard__namedPage',
            attributes: {
                "pageName":"chatter"
            }
        });
    }

}