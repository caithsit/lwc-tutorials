
import { LightningElement,track } from 'lwc';

export default class ParentComponent2 extends LightningElement {
    @track msg;
   constructor()
   {
       super();
       this.template.addEventListener('mycustomevent',this.handleCustomEvent.bind(this));
   }
    handleCustomEvent(event)
    {
        this.msg=event.detail;
    }
}

/*import { LightningElement, track } from 'lwc';

export default class ParentComponentCustomEvents extends LightningElement 
{
    @track msg;

    handleCustomEvent(event)
    {
        console.log('Before');
        this.msg = event.detail;
        console.log('value ' + this.msg);
    }
}*/